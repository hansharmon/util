### CallTable ###
This is a templated call/jump table implementation.
The goal is create functions calls based on a pre-define table lookup.
See CallTableTest.cpp for an example

### Compiling ###

  g++ -o calltabletest CallTableTest.cpp
  g++ -o stringtabletest StringTableTest.cpp

### Running ###
  ./calltabletest

### Comments ###
There are a lot of Jump table references in C, but this is different, so take a look.
Also this could make almost a string switch statement work.


// Create an string table
CallTable<string> command_table;
command_table["hello"] = []() { cout << "hello world"; )};
command_table["goodbye"] = []() {  cout << "goodbye"; exit(0); )};


// Check for a command input
string command;

while (1) {
   cin >> command;
   command_table[command]();
}

