#include "CallTable.h"


void action1(void) {
    cout << "1" << endl;
}

void action2(void) {
    cout << "2" << endl;
}

void xor_test() {
    CallTable<bool,bool> xo;
    xo[false][false] = []() { cout << "false" << endl; };
    xo[false][true ] = []() { cout << "true"  << endl; };
    xo[true ][false] = []() { cout << "true"  << endl; };
    xo[true ][true ] = []() { cout << "false" << endl; };

    bool a,b;
    a = false;
    b = false;
    xo[a][b]();  // Prints "false"
    a = false;
    b = true;
    xo[a][b]();  // Prints "true"
    a = true;
    b = false;
    xo[a][b]();  // Prints "true"
    a = true;
    b = true;
    xo[a][b]();  // Prints "false"
}

int main() {
    xor_test();

    // Create a CallTable
    CallTable<bool,bool,int> ct;
    ct.set_nop([](){ cout << "nop" << endl; });
    // Set actions to take.
    ct[true][false][1] = action1;
    ct[true][true][2] = action2;

    // Call those actions
    bool a = true;
    bool b = false;
    ct[a][b][1]();
    ct[a][b][2]();
    b = true;
    ct[a][b][2]();
    a = false;
    ct[a][b][1]();

}