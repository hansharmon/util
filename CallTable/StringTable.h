/**
 * @file StringTable.h
 * @brief Creates a map of different types of values to a string.
 * 
 * This class template is designed to allow for a functional call table.
 * That is if you want to build a truth table with actions it can be done
 * 
 * 
 * Example (Very Basic):
 *      // Initialization of the call table to set what to call when a state happens.
        StringTable<bool,bool> xo;
        xo[false][false] =  "false" ;
        xo[false][true ] =  "true"  ;
        xo[true ][false] =  "true"  ;
        xo[true ][true ] =  "false" ;

        // During runtime this section is used to make the state function calls.
        bool a,b;
        a = false;
        b = false;
        cout << xo[a][b] << endl;  // Prints "false"
        a = false;
        b = true;
        cout << xo[a][b] << endl;  // Prints "true"
        a = true;
        b = false;
        coutu << xo[a][b] << endl;  // Prints "true"
        a = true;
        b = true;
        cout << xo[a][b] << endl;  // Prints "false"
 * 
 * 
 * TODO:  Make this pattern more generic, maybe use the override template to make it do something on the last entry.
 */

#ifndef _STRING_TABLE_H_
#define _STRING_TABLE_H_

#include<map>
#include<iostream>

using namespace std;

/**
 * @brief Base template for the stringtable.
 * This handles the case of a 0 sized parameter pack.
 * This is the ending class (no arugments) that calls the function for the jump table.
 * A specific template will be used to handle the mapping of arguments.
 * There also exists a nop function if this is a default version of the 
 * base StringTable.  Nop does as it implies and does nothing.
 */
template<typename... Arguments>
class StringTable {      
    public:
        std::string operator=(const StringTable & str) { return value; }
        StringTable& operator=(const std::string &str) { value = str; return *this; }
        operator std::string() { return value; }
        operator const char*() { return value.c_str(); }

    protected:
        std::string value;       //!< No operation value, default to empty function call. 
        

};

/**
 * @brief Specialied template for StringTable.
 * This handles the case of having parameter pack with more than 1 or more entries <t,args...>
 * Used a std::map to lookup the NEXT StringTable until a base level StringTable is readch, then
 * That's function operator will be called.
 * 
 * @tparam T Base
 * @tparam Arguments 
 */
template<typename T, typename... Arguments>
class StringTable<T,Arguments...> {
    public:

        /**
         * @brief operator[] used as a lookup for the next StringTable to call.
         * Internally if the value does not exist in the map this creates a new StringTable and 
         * sets the nop operation.  Finally the StringTable at the Type.
         * 
         * @param t 
         * @return StringTable<Arguments...>& 
         */
        StringTable<Arguments...>& operator[](T t) {
            return my_map[t];
        }

    protected:
        map<T,StringTable<Arguments...>> my_map;          //!< Maps to the next parameter pack.
};


#endif // _STRING_TABLE_H_