#include "LookupTable.h"


void action1(void) {
    cout << "1" << endl;
}

void action2(void) {
    cout << "2" << endl;
}

void xor_string_test() {
    cout << "XOR as a string LookupTable" << endl;
    LookupTable<std::string,bool,bool> xo;
    xo[false][false] =  "false" ;
    xo[false][true ] =  "true"  ;
    xo[true ][false] =  "true"  ;
    xo[true ][true ] =  "false" ;

    // During runtime this section is used to make the state function calls.
    bool a,b;
    a = false;
    b = false;
    cout << xo[a][b].size() << endl;  // Prints "false"
    a = false;
    b = true;
    cout << xo[a][b] << endl;  // Prints "true"
    a = true;
    b = false;
    cout << xo[a][b] << endl;  // Prints "true"
    a = true;
    b = true;
    cout << xo[a][b] << endl;  // Prints "false"
}

void xor_bool_test() {
    cout << "XOR as a bool LookupTable" << endl;
    LookupTable<bool,bool,bool> xo;
    xo[false][false] =  false ;
    xo[false][true ] =  true  ;
    xo[true ][false] =  true  ;
    xo[true ][true ] =  false ;

    // During runtime this section is used to make the state function calls.
    bool a,b;
    a = false;
    b = false;
    cout << xo[a][b].size() << endl;  // Prints "false"
    a = false;
    b = true;
    cout << xo[a][b] << endl;  // Prints "true"
    a = true;
    b = false;
    cout << xo[a][b] << endl;  // Prints "true"
    a = true;
    b = true;
    cout << xo[a][b] << endl;  // Prints "false"
}


void xor_function_test() {
    cout << "XOR as a function LookupTable" << endl;
    LookupTable<function<void(void)>,bool,bool> xo;
    xo[false][false] = []() { cout << "false" << endl; };
    xo[false][true ] = []() { cout << "true"  << endl; };
    xo[true ][false] = []() { cout << "true"  << endl; };
    xo[true ][true ] = []() { cout << "false" << endl; };

    bool a,b;
    a = false;
    b = false;
    xo[a][b]();  // Prints "false"
    a = false;
    b = true;
    xo[a][b]();  // Prints "true"
    a = true;
    b = false;
    xo[a][b]();  // Prints "true"
    a = true;
    b = true;
    xo[a][b]();  // Prints "false"
}


int main() {
    xor_bool_test();
    xor_string_test();
    xor_function_test();
}