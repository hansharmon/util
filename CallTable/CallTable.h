/**
 * @file CallTable.h
 * @brief Template Lookup Table for making function calls.
 * 
 * This class template is designed to allow for a functional call table.
 * That is if you want to build a truth table with actions it can be done
 * 
 * 
 * Example (Very Basic):
 *      // Initialization of the call table to set what to call when a state happens.
        CallTable<bool,bool> xo;
        xo[false][false] = []() { cout << "false" << endl; };
        xo[false][true ] = []() { cout << "true"  << endl; };
        xo[true ][false] = []() { cout << "true"  << endl; };
        xo[true ][true ] = []() { cout << "false" << endl; };

        // During runtime this section is used to make the state function calls.
        bool a,b;
        a = false;
        b = false;
        xo[a][b]();  // Prints "false"
        a = false;
        b = true;
        xo[a][b]();  // Prints "true"
        a = true;
        b = false;
        xo[a][b]();  // Prints "true"
        a = true;
        b = true;
        xo[a][b]();  // Prints "false"
 * 
 * 
 */

#ifndef _CALL_TABLE_H_
#define _CALL_TABLE_H_

#include<map>
#include<iostream>
#include<functional>

using namespace std;

/**
 * @brief Base template for the calltable.
 * This handles the case of a 0 sized parameter pack.
 * This is the ending class (no arugments) that calls the function for the jump table.
 * A specific template will be used to handle the mapping of arguments.
 * There also exists a nop function if this is a default version of the 
 * base CallTable.  Nop does as it implies and does nothing.
 */
template<typename... Arguments>
class CallTable {
    public:
        
        /**
         * @brief Function call operator.
         * For calling the function at the end of the look up chain.
         * 
         */
        void operator()() {
            action();
        }

        /**
         * @brief Default nop function, does nothing.
         */
        static void default_nop() { }

        /**
         * @brief Equal operator used for setting the function to for the call table.
         * 
         * @param act Action function to call when this CallTable is called.
         */
        void operator=(function<void(void)> act) { action = act; }

        /**
         * @brief Set the nop function.
         * This allows for an override of the default nop function.
         * Can be changed to generate an exception or log to a file.
         * 
         * @param nop 
         */
        void set_nop(function<void(void)> nop) {
            no_op = nop;
        }

    protected:
        function<void(void)> no_op = default_nop;       //!< No operation value, default to empty function call. 
        function<void(void)> action = no_op;            //!< Action to take if called, defaults to no operation.
};

/**
 * @brief Specialied template for CallTable.
 * This handles the case of having parameter pack with more than 1 or more entries <t,args...>
 * Used a std::map to lookup the NEXT CallTable until a base level CallTable is readch, then
 * That's function operator will be called.
 * 
 * @tparam T Base
 * @tparam Arguments 
 */
template<typename T, typename... Arguments>
class CallTable<T,Arguments...> {
    public:

        /**
         * @brief Default nop function, does nothing.
         */
        static void default_nop() { }

        /**
         * @brief Equal operator used for setting the function to for the call table.
         * 
         * @param act Action function to call when this CallTable is called.
         */
        void operator=(function<void(void)> act) { action = act; }

        /**
         * @brief Set the nop function.
         * This allows for an override of the default nop function.
         * Can be changed to generate an exception or log to a file.
         * 
         * @param nop 
         */
        void set_nop(function<void(void)> nop) {
            no_op = nop;
        }

        /**
         * @brief operator[] used as a lookup for the next CallTable to call.
         * Internally if the value does not exist in the map this creates a new CallTable and 
         * sets the nop operation.  Finally the CallTalbe at the Type.
         * 
         * @param t 
         * @return CallTable<Arguments...>& 
         */
        CallTable<Arguments...>& operator[](T t) {
            if (my_map.find(t) == my_map.end()) {
                my_map[t].set_nop(no_op);
                my_map[t] = no_op;
            }
            return my_map[t];
        }

    protected:
        map<T,CallTable<Arguments...>> my_map;          //!< Maps to the next parameter pack.
        function<void(void)> no_op = default_nop;       //!< No operation value, default to empty function call. 
        function<void(void)> action = no_op;            //!< Action to take if called, defaults to no operation.
};


#endif // _CALL_TABLE_H_