#include "StringTable.h"


void action1(void) {
    cout << "1" << endl;
}

void action2(void) {
    cout << "2" << endl;
}

void xor_test() {
    StringTable<bool,bool> xo;
    xo[false][false] =  "false" ;
    xo[false][true ] =  "true"  ;
    xo[true ][false] =  "true"  ;
    xo[true ][true ] =  "false" ;

    // During runtime this section is used to make the state function calls.
    bool a,b;
    a = false;
    b = false;
    cout << xo[a][b] << endl;  // Prints "false"
    a = false;
    b = true;
    cout << xo[a][b] << endl;  // Prints "true"
    a = true;
    b = false;
    cout << xo[a][b] << endl;  // Prints "true"
    a = true;
    b = true;
    cout << xo[a][b] << endl;  // Prints "false"
}

int main() {
    xor_test();
}