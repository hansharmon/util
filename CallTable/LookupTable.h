/**
 * @file LookupTable.h
 * @brief Creates a lookup table based on given inputs to generate a given output
 * 
 * This supercedes CallTable and StringTable, but making an even more generic template
 * Specific Templates are then used as the base cases for Strings and Functions to
 * recreate the calltable and stringtable functionality.
 * Think of this as an evolution on the calltable concept.
 * 
 * 
 * Example (Very Basic):
 *      // Initialization of the call table to set what to call when a state happens.
        LookupTable<string,bool,bool> xo;
        xo[false][false] =  "false" ;
        xo[false][true ] =  "true"  ;
        xo[true ][false] =  "true"  ;
        xo[true ][true ] =  "false" ;

        // During runtime this section is used to make the state function calls.
        bool a,b;
        a = false;
        b = false;
        cout << xo[a][b] << endl;  // Prints "false"
        a = false;
        b = true;
        cout << xo[a][b] << endl;  // Prints "true"
        a = true;
        b = false;
        coutu << xo[a][b] << endl;  // Prints "true"
        a = true;
        b = true;
        cout << xo[a][b] << endl;  // Prints "false"
 * 
 * 
 */

#ifndef _LOOKUP_TABLE_H_
#define _LOOKUP_TABLE_H_

#include<map>
#include<iostream>
#include<functional>    // For CallTable functionallity.

using namespace std;

/**
 * @brief Base template for the lookuptable.
 * This handles the case of a 0 sized parameter pack.
 * This is the ending class for the template args.  At its most basic
 * it returns the value stored in the lookup table.
 * A specific template will be used to handle the each column of the table lookup.
 */
template<typename T, typename... Arguments>
class LookupTable {      
    public:

    protected:

};


/**
 * @brief Specialied template for LookupTable.
 * This handles the case of having parameter pack with more than 1 or more entries <t,args...>
 * Used a std::map to lookup the NEXT LookupTable until a base level LookupTable is readch, then
 * That's function operator will be called.
 * 
 * @tparam Type type to store inside this lookup table.
 * @tparam T Base
 * @tparam Arguments 
 */
template<typename Type, typename T, typename... Arguments>
class LookupTable<Type,T,Arguments...> {
    public:

        /**
         * @brief operator[] used as a lookup for the next LookupTable to call.
         * Internally if the value does not exist in the map this creates a new LookupTable and 
         * sets the nop operation.  Finally the LookupTable at the Type.
         * 
         * @param t 
         * @return LookupTable<Arguments...>& 
         */
        LookupTable<Type,Arguments...>& operator[](T t) {
            return my_map[t];
        }

    protected:
        map<T,LookupTable<Type,Arguments...>> my_map;          //!< Maps to the next parameter pack.
};

template<typename Type>
class LookupTable<Type> { 
    public:     
        Type operator=(const LookupTable &lut) { return value; }
        LookupTable& operator=(const Type &v) { value = v; return *this; }
        operator Type() { return value; }
        
    protected:
        Type value;       //!< No operation value, default to empty function call. 
        
}

/**
 * @brief String specific LookupTable.
 * This is needed to provide an end point for std::string and the char* that
 * Makes up most of the use cases for std::string.  Using std::string in the 
 * more generic form leads to a bunch of errors on type casting to char*  this
 * Handles that edge case by being a string, char*, and a LookupTable specific
 * implementation.
 * 
 * @tparam  No parameters, this is a specific std::string end case.
 */
template<>
class LookupTable<std::string> : public std::string {      
    public:
        std::string operator=(const LookupTable & str) { return value; }
        LookupTable& operator=(const std::string &str) { value = str; return *this; }
        operator std::string() { return value; }
        operator const char*() { return this->c_str(); }

   protected:
       std::string value;       //!< No operation value, default to empty function call. 
};


/**
 * @brief Function specific LookupTable
 * Effectively replaces the CallTable, is an end case for a function<void(void)> template
 * Allows a function to be looked up and called.s
 * 
 * @tparam  
 */
template<>
class LookupTable<function<void(void)>> {      
    public:
        function<void(void)> operator=(const LookupTable & lut) { return action; }
        LookupTable& operator=(const function<void(void)> &f) { action = f; return *this; }
        operator function<void(void)>() { return action; }

        /**
         * @brief Default nop function, does nothing.
         */
        static void default_action() { }

       /**
         * @brief Function call operator.
         * For calling the function at the end of the look up chain.
         * 
         */
        void operator()() {
            action();
        }

    protected:
        function<void(void)>  action = default_action;       //!< No operation value, default to empty function call. 
};


#endif // _STRING_TABLE_H_