# README #


### What is this repository for? ###

Small file/class utilities that have been useful to me.

### How do I get set up? ###

There will be a readme with each one-off utility.  
Usually the case will be to use gcc to compile and run the test code.
Not likely to see a make or cmake file to go with the code.

### What's Here ###

* bcd - Binary Code Decimal
* CallTable - A c++ template for jump/call tables
* StackTracer - A c/c++ stack trace on signal singleton


### Contribution guidelines ###

Feel free to email/comment suggestions/alternatives to this code.
Feel free to email/comment on things it could do *better*, maybe some section will get better enough to become it's own repo?
Some might code be re-inventing the wheel.  
Some might code might be for adding to existing code or libraries to make them useful to make.
Be nice, this is in general experimental or rough code, so keep comments positive.  
Code in there has served some purpose in my coding existance.  
As stated, if there is better already existing library or even stack overflow topic, please let me know.

### Who do I talk to? ###

Email hans.p.harmon@gmail.com or robot.cog@gmail.com

### What's is everything in the libary ###
 * CallTable - A C++ Templated Call/Jump Table implementation
 * StackTracer - A stack trace printing signal handler.
 