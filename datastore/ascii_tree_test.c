#include "ascii_tree.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_me(FILE* f,void *input) {
    fprintf(f,"%d\n",*((int32_t*) input));
    free(input);        // hack cleanup.
}

int main(int argc, char * argv[]) {
    ascii_tree * tree = create_ascii_tree();
    if (argc < 2) {
        printf("Usage:\n\t%s <filename>\n", argv[0]);
        return -1;
    }
    FILE * f = fopen(argv[1],"r");
    if (!f) {
        printf ("Cannot open %s\n", argv[1]);
        return -1;
    }
    uint32_t i;
    char *buffer = NULL;
    size_t len = 0;
    int32_t red = getline(&buffer,&len,f);
    int32_t * new_i = NULL;
    
    while (red != -1) {
        new_i = (int32_t*) malloc(sizeof(int32_t));
        *new_i = strlen(buffer) - 2;
        if (new_i > 0) {
            buffer[*new_i] = '\0';   // remove the newline.
            ascii_tree_add(tree,buffer,new_i);
        }
        red = getline(&buffer,&len,f);
    }    
    ascii_tree_print(tree,stdout,print_me);
    fclose(f);
    return 0;
}