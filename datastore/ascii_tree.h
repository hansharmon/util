/*!****************************************************************************
*
*	FILE NAME:
*		ascii_tree.h
*
*	DESCRIPTION:
*		This is a tree that stores words with pointers at the nodes.
*		The words are stored in alphabetical order as one progresses through the
*		the tree, each letter pointing to a new letter node.
*		
*		Example:
*			Store words:  the their monkey
*
*					  /i-r-END
*				 t-h-e
*				/     \END
*			Root
*				\m-o-n-k-e-y-END
*
*			In this manner anything can be looked up by a string with length of string time.
*
*
*
*	NOTES:
*		Uses only caps between A-Z, space and symbols are converted to '_', and '\0' indicates a branch.
*
*
*	\brief ASCII Tree
*
*	\file ascii_tree.h
*
*//*****************************************************************************/


#ifndef __ASCII_TREE_H_
#define __ASCII_TREE_H_

/*!----------------------------------------------------
                  INCLUDE FILES
  ---------------------------------------------------*///

#include <stdint.h>
#include "list.h"		//to store a string stack...
#include <stdio.h>

/*!----------------------------------------------------
                  CONSTANT LITERALS
  ---------------------------------------------------*///
#ifndef NULL
	#define 	NULL 			0
#endif

#define ASCII_TREE_SIZE	256			//!< ALL ASCII Values, except /0 can be stored.


#ifdef __cplusplus
extern "C"
{
#endif

/*!----------------------------------------------------
                      TYPES
  ---------------------------------------------------*///

//! Stores the nodes for the word tree.
typedef struct ascii_tree_node
{
	void * data;									//!< Data stored at the node.
	struct ascii_tree_node * nodes[ASCII_TREE_SIZE];	//!< Links to other nodes.
} ascii_tree_node;

//! contains an entire word tree
typedef ascii_tree_node ascii_tree;

//! An iterator to move through the entire tree.
typedef struct ascii_tree_iterator
{
	p_stack * location_stack;		//!< Stack to be able to back out later from.
	p_stack * node_stack;			//!< Stack of node to pop back out from a certain depth.
} ascii_tree_iterator;

/*!----------------------------------------------------
                   FUNCTION PROTOTYPES
  ----------------------------------------------------*///

/*!**********************************************************
*
*	PROCEDURE NAME:
*		create_ascii_tree_node
*
*	DESCRIPTION:
*		Creates a new node, initialized everything to NULL.  
*		Destroy by calling destroy_ascii_tree_node.
*
*	\return A new tree node.
*//**********************************************************/
ascii_tree_node * create_ascii_tree_node();

/*!**********************************************************
*
*	PROCEDURE NAME:
*		destroy_ascii_tree_node
*
*	DESCRIPTION:
*		Recusively destroyes the node and everything 
*		connected to it, and then itself.
*
*	\param node pointer to the node to destroy.
*//**********************************************************/
void destroy_ascii_tree_node(ascii_tree * node);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		create_ascii_tree
*
*	DESCRIPTION:
*		Creates a new ascii_tree, destroy by calling destroy_ascii_tree.
*		Same as create_ascii_tree_node
*		
*	\return A pointer to a new word tree.
*//**********************************************************/
ascii_tree * create_ascii_tree();

/*!*****************************************************
*	PROCEDURE NAME:
*		copy_ascii_tree
*
*	DESCRIPTION:
*		Create a duplicate ascii tree.
*
*	\brief
*		Create a duplicate ascii tree.
*
*//******************************************************/
ascii_tree * copy_ascii_tree( ascii_tree * tree );

/*!**********************************************************
*
*	PROCEDURE NAME:
*		destroy_ascii_tree
*
*	DESCRIPTION:
*		Destroys the word tree by taking out its root node.
*		Same as destory_ascii_tree_node.
*		
*	\param tree Pointer to the word tree to destroy.
*//**********************************************************/
void destroy_ascii_tree(ascii_tree * tree);

/*!*****************************************************
*	PROCEDURE NAME:
*		ascii_tree_clear
*
*	DESCRIPTION:
*		clears out the ascii tree.
*
*	\brief
*		clears out the ascii tree.
*
*	\param tree Pointer to the word tree to destroy.
*
*//******************************************************/
void ascii_tree_clear ( ascii_tree * tree );

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_add
*
*	DESCRIPTION:
*		Adds the data into the word tree at the string location,.
*		If there was something stored there already it returns that value, replacing this.
*		Be sure to NULL termniate any string added,			 ()()	
*		Least ye suffer the wrath of the SEGFAULT monster.  (>`v')>
*		If a string is not in uppercase with no white space or 
*		symbols it will be converted before being added.
*
*	\param node 	Tree or node to add the string and data to.
*	\param string 	Array of characters to add to the tree.
*	\param data		Pointer to information to store here.
*
*	\return NULL 	if no value present; otherwise the pointer to the data stored.
*//**********************************************************/
void * ascii_tree_add(ascii_tree * tree,uint8_t * str,void * data);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_has_word
*
*	DESCRIPTION:
*	Checks for the word in the tree, returns true 
*	if there is already an entry for this tree.
*
*	\param tree		Tree to check for the word.
*	\param string	Word to check in the tree.
*
*	\return 0 if not found, 1 if found.
*//**********************************************************/
uint8_t ascii_tree_has_word(ascii_tree * tree,uint8_t * str);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_get
*
*	DESCRIPTION:
*		Returns the data stored at the given string.
*
*	\param tree		Tree to get the data from.
*	\param string	Word to lookup the data with.
*	
*	\return			A pointer to the data or NULL
*//**********************************************************/
void * ascii_tree_get(ascii_tree * tree, const uint8_t * str);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_get_char_index
*
*	DESCRIPTION:
*		Returns which index this uint8_t maps to.
*		This really should never be seen by a user.
*		It is an internal function to help with storage.
*
*	\param c	Charactor to get an index value for.
*
*	\return A number bettwen -1 and ASCII_TREE_SIZE
*//**********************************************************/
int16_t ascii_tree_get_char_index(uint8_t c);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_get_char_value
*
*	DESCRIPTION:
*		Returns the char related to an index.
*		This really should never be seen by a user.
*		It is an internal function to help with storage.
*		
*	\param i	Index to convert to a charactor.
*
*	\return The charactor value for the given index.
*//**********************************************************/
uint8_t ascii_tree_get_char_value(int16_t i);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_print
*
*	DESCRIPTION:
*	Dumps the tree to the FILE
*	Use stdout for output to screen.
*	Requires a function to use for writing out the void *
*	Output is of the form:
*		"KEY = " + print_me(void *);
*
*	\param tree 		Tree to print out.
*	\param output		Where to print the tree out to.
*	\param print_me		Function used to print the data.
*//**********************************************************/
void ascii_tree_print(ascii_tree * tree,FILE * output,void (*print_me)(FILE*,void *));

/*!**********************************************************
*
*	PROCEDURE NAME:
*		_ascii_tree_print
*
*	DESCRIPTION:
*		Helper function for ascii_tree_print.
*		Used the string_stack to track where in the tree
*		the printing currently is.
*
*	\param tree 		Tree to print out.
*	\param string_stack	String stored in a list to hold current position.
*	\param output		Where to print the tree out to.
*	\param print_me		Function used to print the data.
*
*//**********************************************************/
void _ascii_tree_print(ascii_tree * tree,list * string_stack,FILE * output,void (*print_me)(FILE*,void *));

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_print_string_stack
*
*	DESCRIPTION:
*		Used to print out the tree.
*
*	\param output			Where to print the string to.
*	\param string_stack		What to print, should be a pointer to a uint8_t.
*
*//**********************************************************/
void ascii_tree_print_string_stack(FILE * output,void * string_stack);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_apply
*
*	DESCRIPTION:
*		Used to applies a function to the entire tree.
*
*	\param tree				Tree to plow through
*	\param function			Function to apply to the tree.
*
*//**********************************************************/
void ascii_tree_apply(ascii_tree * tree,void (*function) (void *));

/*!-----------------------------------------------------------------------------
                               ITERATOR FUNCTIONS
-----------------------------------------------------------------------------*///

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_create_iterator
*
*	DESCRIPTION:
*		Creates a iterator to move through the tree with.
*		Destroy using destroy_ascii_tree_iterator, not free.
*
*//**********************************************************/
ascii_tree_iterator * create_ascii_tree_iterator(ascii_tree * tree);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		destroy_ascii_tree_iterator
*
*	DESCRIPTION:
*		Destroys the iterator.
*
*//**********************************************************/
void destroy_ascii_tree_iterator(ascii_tree_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_iterator_next
*
*	DESCRIPTION:
*		Returns the next element in the word tree.
*
*	\param iterator
*
*	\return A pointer to the next element.
*//**********************************************************/
void * ascii_tree_iterator_next(ascii_tree_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_iterator_get_location
*
*	DESCRIPTION:
*		Returns the string of where the word tree is.
*		You are responcible for destroying that string.
*
*//**********************************************************/
void ascii_tree_iterator_get_location(ascii_tree_iterator * iterator,uint8_t * str, uint32_t n );

uint32_t ascii_tree_iterator_get_depth ( ascii_tree_iterator * iterator );

#ifdef __cplusplus
}
#endif

#endif //__ASCII_TREE_H_

