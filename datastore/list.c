
/******************************************************************************
*
*	FILE NAME:
*		list.c
*
*	DESCRIPTION:
*	
*   This is a very simple queue, stack, and list that holds anything.
*	O(1) add, push, pop, enqueue, and dequeue.
*	O(n) insert, erase, get.
*   
*	NOTES:
*
*	\brief Double Linked List
*
*	\file list.c
*
******************************************************************************/

/*----------------------------------------------------
                  INCLUDE FILES
  ---------------------------------------------------*/

#include "list.h"
#include <stdlib.h>

/*----------------------------------------------------
                  FUNCTION IMPLEMENTATION
  ---------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/***********************************************************
*
*	PROCEDURE NAME:
*		create_node
*
*	DESCRIPTION:
*		Creates a blank node and returns it.
*		To destroy just use free(node).
*
*	\return A new list_node pointer.
***********************************************************/
list_node * create_list_node()
{
	list_node * node;						//Node to return.
	
	node = (list_node*) malloc(sizeof(list_node));		//Get Memory
	node->data		= NULL;				//Set everthing to NULL.
	node->next		= NULL;
	node->previous	= NULL;

	return node;
}

/*---------------------------------------------------------
 *                COMMON list FUNCTIONS
 *---------------------------------------------------------*/

/***********************************************************
*
*	PROCEDURE NAME:
*		create_list
*
*	DESCRIPTION:
*		Creates a pointer to a list
*
*	\returns a pointer to a list.
***********************************************************/
list * create_list()
{
	list * l;						//List to return
	
	l = (list*) malloc(sizeof(list));		//Get memory

	if ( l == NULL ) return NULL;

	l->head = NULL;					//Set head to NULL
	l->count = 0;					//Have no items.
#ifndef NON_ANSI_COMPLIANT
	l->iterator = create_list_iterator(l);
#endif

	return l;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		destroy_list
*
*	DESCRIPTION:
*		Free memory associated with the list.
*
*	\param list	The list to clean up.
***********************************************************/
void destroy_list(list * l)
{
	if ( l == NULL ) return;

	list_clear(l);		//Clean all of nodes
#ifndef NON_ANSI_COMPLIANT
	destroy_list_iterator(l->iterator); // Destroy the iterator.
#endif
	free(l);				//Free memory
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_clear
*
*	DESCRIPTION:
*		Deletes all of the nodes within this list.
*
*	\param list	List to free all of the nodes
***********************************************************/
void list_clear(list * l)
{
	//If null, we're done.
	while(l->head != NULL)	
	{
		dequeue(l);
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_is_empty
*
*	DESCRIPTION:
*		Returns if the list is empty.
*
*	\param list	List to check for emptiness.
*
*	\return true if the list is empty.
***********************************************************/
uint8_t list_is_empty(list * l)
{
	return (l->head == NULL);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_get_count
*
*	DESCRIPTION:
*		Gets the number of items stored in the list.
*
*	\param list	List to get the count from.
*
*	\return The number of items in the list.
***********************************************************/
uint32_t list_get_count(list * l)
{
	return l->count;
}


/***********************************************************
*
*	PROCEDURE NAME:
*		list_get
*
*	DESCRIPTION:
*		Returns the data found at index or NULL if not found.
*
*	\param list 	List to get an item from.
*	\param index	Index into the list get the item.
*
*	\return A pointer to the item or NULL if not found.
***********************************************************/
void * list_get(list * l,uint32_t index) 
{
	list_node * node;
	
	node = list_get_node(l,index);
	
	//If the index is out of range return NULL.
	//Otherwise return the data at the node.
	if (node == NULL)
	{
		return NULL;	
	}
	else
	{
		return node->data;
	}
}


/***********************************************************
*
*	PROCEDURE NAME:
*		list_whereis
*
*	DESCRIPTION:
*		Returns the location of the item given, or -1 if the item is not found.
*
*	\param list 	List to get a node from.
*	\param data		Item to find in the list.
*
*	\return A pointer to the node or NULL if not found.
***********************************************************/
int32_t list_whereis(list * l,void * data)
{
	list_iterator * iterator;
	void * current;
	int32_t index;

	current = NULL;
	
	// Safety check.
	if (l == NULL || data == NULL)
	{
		return -1;
	}

	// Initialize the count
	index = 0;

	// Iterate through the values.
	iterator = create_list_iterator(l);

	while (list_iterator_has_next(iterator))
	{
		current = list_iterator_next(iterator);
		if (current == data)
		{
			return index;
		}
	}

	// Did not find it.
	return -1;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_get_node
*
*	DESCRIPTION:
*		Returns the node found at index or NULL if not found.
*		Helper for a bunch of functions.
*		Should not be used directly.
*
*	\param list 	List to get a node from.
*	\param index	Index into the list get the node.
*
*	\return A pointer to the node or NULL if not found.
***********************************************************/
list_node * list_get_node(list * l,uint32_t index)
{
	list_node * node;
	uint32_t i;
	i = 0;

	node = l->head;				//Start at the head.
	while (i < index)				//Loop to the index.
	{
		if (node == NULL) 			//NULL nodes are bad.
		{
			return NULL;			//So return NULL.
		}
		node = node->next;			//Move on to the next node.
		i = i + 1;					//Update counter.
		if (node == l->head)		//Full circle check.
		{
			return NULL;			//Index is too high return NULL.
		}
	}
	return node;					//Found so returning.
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_insert
*
*	DESCRIPTION:
*		Inserts data at the given index, 
*		into the list, or at the last available possition.
*		All other data is pushed back.
*
*	\param list		List to insert an item into.
*	\param index	Where to insert at.
*	\param data		Pointer to data to store.
***********************************************************/
void list_insert(list * l, uint32_t index,void * data)
{
	list_node * node;
	list_node * new_node;

	
	node = list_get_node(l,index);			//Goto the node.
	if (node != NULL)							
	{
		new_node = (list_node*) malloc(sizeof(list_node));		//Make a new node.

		new_node->next = node;					//Drop into place
		new_node->previous = node->previous;
		new_node->data = data;

		node->previous->next = new_node;		
		node->previous = new_node;

		if (index == 0)							//If this is a new head, check that.
		{
			l->head = new_node;
		}

		l->count = l->count + 1;			//Increase count.
	}
	else
	{
		enqueue(l,data);
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_erase
*
*	DESCRIPTION:
*		Removes the data at the given index from the list, 
*		returns the item or NULL if not found.
*
*	\param list		List to remove an item from.
*	\param index	Index at which to remove the item.
***********************************************************/
void * list_erase(list * l, uint32_t index)
{
	list_node * node;
	void * data;
	
	node = list_get_node(l,index);		//Go to the index node.

	if (node != NULL)
	{
		if (node->next != node)				//If not pointing to itself (only one element left.)
		{
			if (l->head == node)			//If it is the head node, correct the head first.
			{
				l->head = node->next;
			}

			node->next->previous = node->previous;		//Fix pointers.
			node->previous->next = node->next;
			data = node->data;
		}
		else {								//Last element left, so clear the head.
			data = l->head->data;
			l->head = NULL;
		}
		free(node);							//Free memory

		l->count = l->count - 1;		//Decrement count.

		return data;						//Return the data stored.
	}
	else
	{
		return NULL;						//Not found return NULL
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_add
*
*	DESCRIPTION:
*		Adds the data to the list/queue/statck.
*		Same as enqueue.
*
*	\param list		List to add another item to.
*	\param data		Item to add to the list.
***********************************************************/
void list_add(list * l,void * data)
{
	enqueue(l,data);
}

// Some compliers cannot pass functions.
#ifndef NON_ANSI_COMPLIANT
/***********************************************************
*
*	PROCEDURE NAME:
*		list_print
*
*	DESCRIPTION:
*		Print everything out to file.
*		Use stdout to print to screen.
*
*	\param list		List to print.
*	\param output	Where to print to.
*	\param print_me	Function used to print the data.
***********************************************************/
void list_print(list * l,FILE* output,void (*print_me)(FILE*,void*))
{
	list_node * node;

	if (l == NULL)
	{
		return;
	}

	node = l->head;			//Start at the top.
	if (node == NULL)
	{
		return;
	}

	//Go through the list printing out whatever is there.
	do {
		print_me(output,node->data);
		node = node->next;
	} while (node != l->head);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_apply
*
*	DESCRIPTION:
*		Calls the given function on elements in the list.
*
*	\param list			List to apply function to.
*	\param function		Function to apply to the list
***********************************************************/
void list_apply(list * l,void (*function)(void *))
{
	list_node * node;

	node = NULL;

	if (l == NULL) return;
	if ( l->head == NULL ) return;

	node = l->head;			//Start at the top.
	if (node == NULL)
	{
		return;
	}

	//Go through the list appling the function.
	do {
		function(node->data);
		node = node->next;
	} while (node != l->head);
}

#endif // NON_ANSI_COMPLIANT

/*---------------------------------------------------------
 *                 QUEUE SPECIFIC FUNCTIONS
 *---------------------------------------------------------*/

/***********************************************************
*
*	PROCEDURE NAME:
*		enqueue
*
*	DESCRIPTION:
*		Adds the data to the end of the queue.
*		O(1) complexity for insert.
*
*	\param queue 	Queue to add an item to.
*	\param data		Data to add to the queue.
***********************************************************/
void enqueue (p_queue * queue, void * data)
{
	list_node * new_node;

	if (data == NULL)
	{
		return;
	}	

	if (queue->head == NULL)
	{
		queue->head = (list_node*) malloc(sizeof(list_node));
		queue->head->data = data;
		queue->head->next = queue->head;
		queue->head->previous = queue->head;
	}
	else if (queue->head->next == queue->head)
	{
		new_node = (list_node*) malloc(sizeof(list_node));
		new_node->next = queue->head;
		new_node->previous = queue->head;
		new_node->data = data;

		queue->head->next = new_node;
		queue->head->previous = new_node;
	}
	else
	{
		new_node = (list_node*) malloc(sizeof(list_node));
		new_node->data = data;

		new_node->next = queue->head;
		new_node->previous = queue->head->previous;

		queue->head->previous->next = new_node;
		queue->head->previous = new_node;
	}
	queue->count = queue->count + 1;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		dequeue
*
*	DESCRIPTION:
*		Removes the first entry in the queue
*
*	\param queue	Queue to remove the first in value from.
*	
*	\return A point to the last item stored here.
***********************************************************/
void * dequeue(p_queue * queue)
{
	list_node * node;
	void * data;

	data = list_top(queue);

	if (queue->head == NULL)
	{
		return NULL;
	}

	//Single item condition.
	else if (queue->head->next == queue->head)
	{
		free(queue->head);
		queue->head = NULL;
	}
	else
	{
		node = queue->head;

		queue->head->next->previous = queue->head->previous;
		queue->head->previous->next = queue->head->next;
		queue->head = queue->head->next;
		
		free(node);
	}
	queue->count = queue->count - 1;

	return data;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_top
*
*	DESCRIPTION:
*		Returns the first element in the queue.
*
*	\param queue 	Queue to get the front of, or top of stack,etc.
*
*	\return A pointer to the data pointed to by the head node.
***********************************************************/
void * list_top(p_queue * queue)
{
	if (queue->head != NULL)
	{
		return queue->head->data;
	}
	return NULL;
}

/*----------------------------------------------------------
 *				STACK FUNCTIONS
 *----------------------------------------------------------*/

/***********************************************************
*
*	PROCEDURE NAME:
*		stack_push
*
*	DESCRIPTION:
*		Pushes an item to the front of the list.
*		Same as insert 0.
*
*	\param stack	Stack to push an item onto.
*	\param data		Data to push on to the stack.
***********************************************************/
void stack_push(p_stack * stack,void * data)
{
	list_insert(stack,0,data);
}
/***********************************************************
*
*	PROCEDURE NAME:
*		stack_pop
*
*	DESCRIPTION:
*		Pop the last item added.
*		Same as dequeue.
*
*	\param stack	Stack to pop the first item off of.
*	
*	\return A pointer to the first item in the stack.
***********************************************************/
void * stack_pop(p_stack * stack)
{
	return dequeue(stack);
}


/*-----------------------------------------------------------------------------
                               ITERATOR FUNCTIONS.
-----------------------------------------------------------------------------*/

/***********************************************************
*
*	PROCEDURE NAME:
*		create_list_terator
*
*	DESCRIPTION:
*		Creates a new list iterator.
*
*	\param list	List to create an iterator for.
***********************************************************/
list_iterator * create_list_iterator(list * l)
{
	list_iterator * iterator;			//Iterator to return

	if (l == NULL) 
	{
		return NULL;
	}
	
	iterator = (list_iterator *) malloc(sizeof(list_iterator));
	
	if (iterator == NULL) 
	{
		return NULL;
	}
	
	iterator->list	 	= l;
	iterator->current	= NULL;
	return iterator;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		destroy_list_iterator
*
*	DESCRIPTION:
*		Destroys a list iteratot, can just use free.
*
***********************************************************/
void destroy_list_iterator(list_iterator * iterator)
{
	free(iterator);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_rewind
*
*	DESCRIPTION:
*		returns the list iterator to initial state.
*
*	\param iterator Iterator to rewind.
*
***********************************************************/
void list_iterator_rewind(list_iterator * iterator)
{
	iterator->current	= NULL;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_has_next
*
*	DESCRIPTION:
*		Check to see if there is a next element in the list.
*
*	\param iterator Iterator to check for next element.
*
*	\return 1 if the list has another element 0 if there are no more elements.
***********************************************************/
uint8_t list_iterator_has_next(list_iterator * iterator)
{
	if (iterator == NULL)
	{
		return 0;
	}

	if (iterator->list->head == NULL)
	{
		return 0;
	}

	if (iterator->current == NULL)
	{
		return 1;
	}

	return (iterator->current->next != iterator->list->head);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_next
*
*	DESCRIPTION:
*		Gets the next element in the list or returns NULL if there is nothing left.
*
*	\param iterator Iterator to next set of data.
*
*	\return NULL if there is nothing left
***********************************************************/
void * list_iterator_next(list_iterator * iterator)
{
	if (iterator == NULL)
	{
		return NULL;
	}

	if (iterator->list->head == NULL)
	{
		return NULL;
	}
	
	if (iterator->current == NULL)
	{
		iterator->current = iterator->list->head;
		return iterator->current->data;
	}
	
	if (list_iterator_has_next(iterator))
	{
		iterator->current = iterator->current->next;
		return iterator->current->data;
	}
	return NULL;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_has_previous
*
*	DESCRIPTION:
*		Check for previous elements.
*
*	\param iterator iterator to check for previous values.
*
*	\retrun true if the iterator is not pointing at the head of the list.
***********************************************************/
uint8_t list_iterator_has_previous(list_iterator * iterator)
{
	if (iterator == NULL)
	{
		return 0;
	}

	if (iterator->current == NULL)
	{
		return 0;
	}

	return (iterator->current != iterator->list->head);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		lsit_iterator_previous
*
*	DESCRIPTION:
*		Retruns the previous element, moves the pointer.
*
*	\param iterator iterator to get the previous element or NULL if there is none.
***********************************************************/
void * list_iterator_previous(list_iterator * iterator)
{
	void * data;

	if (list_iterator_has_previous(iterator))
	{
		iterator->current = iterator->current->previous;
		data = iterator->current->data;

		if (iterator->current == iterator->list->head)
		{
			iterator->current = NULL;
		}
		return data;
	}
	return NULL;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_current
*
*	DESCRIPTION:
*		Returns the current element being looked at.
*
*	\param iterator Iterator to get the current element.
*
*	\return The current element, or NULL if there are problems.
***********************************************************/
void * list_iterator_current(list_iterator * iterator)
{
	if (iterator != NULL)
	{
		return iterator->current;
	}
	return NULL;
}


/***********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_revmoe_current
*
*	DESCRIPTION:
*		 Removes the current item from the list.*
*	\param iterator Iterator to get the current element.
*
*	\return The current element, or NULL if there are problems.
***********************************************************/
void * list_iterator_remove_current(list_iterator * iterator)
{
	//return list_erase(iterator->list,list_whereis(iterator->list,iterator->current));
	
	list_node * previous;
	list_node * next;
	void * data;

	if (iterator == NULL)
	{
		return NULL;
	}

	if (iterator->current == NULL)
	{
		return NULL;
	}

	// if this is the head or possible the last one, just use dequeue.
	if (iterator->current == iterator->list->head)
	{
		return dequeue(iterator->list);
	}

	// Save state
	previous	= iterator->current->previous;
	next		= iterator->current->next;
	data		= iterator->current->data;

	// Set the next and previous pointers.
	next->previous = previous;
	previous->next = next;

	// free the memory used.
	free(iterator->current);

	// Set to the previous value.
	iterator->current = previous;

	// Return the data stored with in
	return data;
}

#ifdef __cplusplus
}
#endif

