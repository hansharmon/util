/*!*****************************************************************************
*
*	FILE NAME:
*		ascii_tree.h
*
*	DESCRIPTION:
*		This is a tree that stores words with pointers at the nodes.
*		The words are stored in alphabetical order as one progresses through the
*		the tree, each letter pointing to a new letter node.
*		
*		Example:
*			Store words:  the their monkey
*
*					  /i-r-END
*				 t-h-e
*				/     \END
*			Root
*				\m-o-n-k-e-y-END
*
*			In this manner anything can be looked up by a string with length of string time.
*
*
*
*	NOTES:
*		Uses only caps between A-Z and space is converted to '_', and '\0' Indicates a branch.
*
*	\brief Word Tree
*
*	\file ascii_tree.c
*
*
*//*****************************************************************************/

#include "ascii_tree.h"
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

/***********************************************************
*
*	PROCEDURE NAME:
*		create_ascii_tree_node
*
*	DESCRIPTION:
*		Creates a new node, initialized everything to NULL.  
*		Destroy by calling destroy_ascii_tree_node.
*
*	\return A new tree node.
***********************************************************/

ascii_tree_node * create_ascii_tree_node()
{
	int16_t i;					//Used to initialize node pointers.
	ascii_tree_node * node;		//Node to create.
	
	//Get memory.
	node = (ascii_tree_node*) malloc(sizeof(ascii_tree_node));
	
	if ( node == NULL ) return NULL;

	//Set the data to NULL.
	node->data = NULL;
	
	//Loop through the node pointers and set to NULL.
	for (i=0;i<ASCII_TREE_SIZE;i++)
	{
		node->nodes[i] = NULL;
	}
	return node;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		destroy_ascii_tree_node
*
*	DESCRIPTION:
*		Recusively destroyes the node and everything 
*		connected to it, and then itself.
*
*	\param node Pointer to the node to destroy.
***********************************************************/

void destroy_ascii_tree_node(ascii_tree_node * node)
{
	int i;						//Used for looping through the nodes.
		
	//If node is NULL we're done.
	if (node == NULL)
	{
		return;
	}
	
	//Loop through all of the nodes.
	for (i=0;i<ASCII_TREE_SIZE;i++)
	{
		destroy_ascii_tree_node(node->nodes[i]);		//Recurse
	}

	//Clean up.
	free (node);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		create_wrod_tree
*
*	DESCRIPTION:
*		Creates a new ascii_tree, destroy by calling destroy_ascii_tree.
*		Same as create_ascii_tree_node
*		
*	\return A pointer to a new word tree.
***********************************************************/
ascii_tree * create_ascii_tree()
{
	return create_ascii_tree_node();
}

/*!*****************************************************
*	PROCEDURE NAME:
*		copy_ascii_tree
*
*	DESCRIPTION:
*		Create a duplicate ascii tree.
*
*	\brief
*		Create a duplicate ascii tree.
*
*//******************************************************/
ascii_tree * copy_ascii_tree( ascii_tree * tree )
{
	ascii_tree *			copy;
	ascii_tree_iterator *	iterator;
	void *					whatever;
	uint8_t					location [ 1024 ];

	if ( tree == NULL ) return NULL;

	copy = create_ascii_tree();
	if ( copy != NULL )
	{
		iterator = create_ascii_tree_iterator ( tree );
		
		do
		{
			whatever = ascii_tree_iterator_next ( iterator );
			if ( whatever != NULL )
			{
				ascii_tree_iterator_get_location ( iterator, location, 1024 );
				ascii_tree_add ( copy, location, whatever );
			}
		}
		while ( whatever != NULL );
		destroy_ascii_tree_iterator ( iterator );

	}
	return copy;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		destroy_ascii_tree
*
*	DESCRIPTION:
*		Destroys the word tree by taking out it's root node.
*		Same as destory_ascii_tree_node.
*		
*	\param tree Pointer to the word tree to destroy.
***********************************************************/
void destroy_ascii_tree(ascii_tree * tree)
{
	destroy_ascii_tree_node(tree);
}

/*!*****************************************************
*	PROCEDURE NAME:
*		ascii_tree_clear
*
*	DESCRIPTION:
*		clears out the ascii tree.
*
*	\brief
*		clears out the ascii tree.
*
*	\param tree Pointer to the word tree to destroy.
*
*//******************************************************/
void ascii_tree_clear ( ascii_tree * tree )
{
	int i;						//Used for looping through the nodes.
		
	//If node is NULL we're done.
	if ( tree == NULL )
	{
		return;
	}
	
	//Loop through all of the nodes.
	for (i=0;i<ASCII_TREE_SIZE;i++)
	{
		destroy_ascii_tree_node( tree->nodes[i] );		//Recurse
		tree->nodes [ i ] = NULL;
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_add
*
*	DESCRIPTION:
*		Adds the data into the word tree at the string location,
*		If there was something stored there already it returns that value, replacing this.
*		Be sure to NULL termniate any string added.
*		Least ye suffer the rath of the SEGFAULT monster.
*		If a string is not in uppercase with no white space or 
*		symbols it will be converted before being added.
*
*	\param node 	Tree or node to add the string and data to.
*	\param string 	Array of charactors to add to the tree.
*	\param data		Pointer to information to store here.
*
*	\return NULL if no value present or the pointer to the data stored.
***********************************************************/
void * ascii_tree_add(ascii_tree * node,uint8_t * str,void * data)
{
	uint8_t c;			//Charactor to index with.
	int16_t index;		//Index into the nodes array.
	void * stored;		//Pointer to data stored in the node.
	
	c = str[0];		//Take the first charactor.
	stored = NULL;		//Set data retrieved to null.
	
	//Lookup the index.
	index = ascii_tree_get_char_index(c);
	
	//If end of string add the data here and be done.
	if (index == -1)
	{
		//If there is data stored return it.
		if (node->data != NULL)
		{
			stored = node->data;
		}
		node->data = data;
		return stored;
	}
	else
	{
		//Check to make sure that there is a node to search.
		//If not create a new node to continus the adding.
		if (node->nodes[index] == NULL)
		{
			node->nodes[index] = create_ascii_tree_node();
		}
		//Recurse
		return ascii_tree_add(node->nodes[index],str+sizeof(uint8_t),data);
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_has_word
*
*	DESCRIPTION:
*	Checks for the word in the tree, returns true 
*	if there is already an entry for this tree.
*
*	\param tree		Tree to check for the word.
*	\param string	Word to check in the tree.
*
*	\return 0 if not found, 1 if found.
***********************************************************/
uint8_t ascii_tree_has_word(ascii_tree * tree,uint8_t * str)
{
	return (ascii_tree_get(tree,str) != NULL);
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_get
*
*	DESCRIPTION:
*		Returns the data stored at the given string.
*
*	\param tree		Tree to get the data from.
*	\param string	Word to lookup the data with.
*	
*	\return	A pointer to the data or NULL
***********************************************************/
void * ascii_tree_get(ascii_tree * tree, const uint8_t * str)
{
	uint8_t c;			//Charactor to index with.
	int16_t index;		//Index into the nodes array.
	uint32_t i;
	uint32_t string_length;
	ascii_tree * my_tree;
	const uint8_t * my_string;

	if ( tree == NULL ) return NULL;
	if ( str == NULL ) return NULL;

	//Init Variables.
	string_length	= strlen((const char*) str);
	my_tree			= tree;
	my_string		= str;

	for (i=0;i<string_length+1;i++)
	{
		// If node is non-existant return NULL
		if (my_tree == NULL || my_string == NULL)
		{
			return NULL;
		}
	
		// Get the proper index for the next iteratation.
		c = my_string[0];

		// End of string
		if (c == '\0')
		{
			index = -1;
		}
		// All other symbols and what not.
		else
		{
			index = c;
		}
		
		//End of string return the data pointer. (it can be NULL)	
		if (index == -1)
		{
			return my_tree->data;
		}
		else
		{
			my_tree = my_tree->nodes[index];
			my_string++;			
		}
	}
	return NULL;
}


/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_get_char_index
*
*	DESCRIPTION:
*		Returns which index this uint8_t maps to.
*		This really should never be seen by a user.
*		It is an internal function to help with storage.
*
*	\param c	Charactor to get an index value for.
*
*	\return A number bettwen -1 and ASCII_TREE_SIZE
***********************************************************/
int16_t ascii_tree_get_char_index(uint8_t c)
{
	//End of string
	if (c == '\0')
	{
		return -1;
	}
	else 
	{
		return c;
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_get_char_value
*
*	DESCRIPTION:
*		Returns the char related to an index.
*		This really should never be seen by a user.
*		It is an internal function to help with storage.
*		
*	\param i	Index to convert to a charactor.
*
*	\return The charactor value for the given index.
***********************************************************/
uint8_t ascii_tree_get_char_value(int16_t i)
{
	//End of string.
	if (i == -1)
	{
		return '\0';
	}
	else 
	{
		return (uint8_t) i;
	}
}


/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_print
*
*	DESCRIPTION:
*	Dumps the tree to the FILE
*	Use stdout for output to screen.
*	Requires a function to use for writing out the void *
*	Output is of the form:
*		"KEY = " + print_me(void *);
*
*	\param tree 		Tree to print out.
*	\param output		Where to print the tree out to.
*	\param print_me		Function used to print the data.
*
***********************************************************/
void ascii_tree_print(ascii_tree * tree,FILE * output,void (*print_me)(FILE*,void *))
{
	list * string_stack;			//Used to store the current location in the tree.

	string_stack = create_list();	//Initialize the list.

	//Call the helper function.
	_ascii_tree_print(tree,string_stack,output,print_me);

	destroy_list(string_stack);		//Clean up.
}

/***********************************************************
*
*	PROCEDURE NAME:
*		_ascii_tree_print
*
*	DESCRIPTION:
*		Helper function for ascii_tree_print.
*		Used the string_stack to track where in the tree
*		the printing currently is.
*
*	\param tree 		Tree to print out.
*	\param string_stack	String stored in a list to hold current position.
*	\param output		Where to print the tree out to.
*	\param print_me		Function used to print the data.
*
***********************************************************/
void _ascii_tree_print(ascii_tree * tree,list * string_stack,FILE * output,void (*print_me)(FILE*,void *))
{
	int16_t i;
	uint8_t c;
	
	//Null node, stop.
	if (tree == NULL)
	{
		return;
	}
	//There is data at this node, so print it out.
	if (tree->data != NULL)
	{
		list_print(string_stack,output,ascii_tree_print_string_stack);
		fprintf(output," = ");
		(*print_me)(output,tree->data);
	}

	//Recurse through the other nodes.
	for (i=0;i<ASCII_TREE_SIZE;i++)
	{
		//Get the current charactor value.
		c = ascii_tree_get_char_value(i);
		//Add the charactor to the end of the current string.
		enqueue(string_stack,&c);
		//Recurse
		_ascii_tree_print(tree->nodes[i],string_stack,output,print_me);
		//Remove the last charactor added.
		list_erase(string_stack,list_get_count(string_stack)-1);
		
	}
}


/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_print_string_stack
*
*	DESCRIPTION:
*		Used to print out the tree.
*
*	\param output			Where to print the string to.
*	\param string_stack		What to print, should be a pointer to a uint8.
*
***********************************************************/
void ascii_tree_print_string_stack(FILE * output,void * string_stack)
{
	fprintf(output,"%c",*((uint8_t *) string_stack));
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_apply
*
*	DESCRIPTION:
*		Used to applies a function to the entire tree.
*
*	\param tree				Tree to plow through
*	\param function			Function to apply to the tree.
*
***********************************************************/
void ascii_tree_apply(ascii_tree * tree,void (*function) (void *))
{
	uint32_t i;

	if ( tree == NULL ) return;

	// If the node is not null.
	// Apply the function to the data
	if (tree->data != NULL)
	{
		function(tree->data);
	}
	
	// Recurse through the tree.
	for (i=0;i<ASCII_TREE_SIZE;i++)
	{
		ascii_tree_apply(tree->nodes[i],function);
	}
}


/*-----------------------------------------------------------------------------
                               ITERATOR FUNCTIONS
-----------------------------------------------------------------------------*/

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_create_iterator
*
*	DESCRIPTION:
*		Creates a iterator to move through the tree with.
*		Destroy using destroy_ascii_tree_iterator, not free.
*
***********************************************************/
ascii_tree_iterator * create_ascii_tree_iterator(ascii_tree * tree)
{
	ascii_tree_iterator * iterator;		//Iterator to return
	uint16_t * index;						//First index to stack.
	
	iterator = (ascii_tree_iterator *) malloc(sizeof(ascii_tree_iterator));
	if ( iterator == NULL ) return NULL;

	iterator->location_stack = create_list();
	iterator->node_stack	 = create_list();

	index = (uint16_t*) malloc(sizeof(uint16_t));
	(*index) = -1;
	stack_push(iterator->location_stack,index);
	stack_push(iterator->node_stack,tree);
	
	return iterator;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		destroy_ascii_tree_iterator
*
*	DESCRIPTION:
*		Destroys the iterator.
*
***********************************************************/
void destroy_ascii_tree_iterator(ascii_tree_iterator * iterator)
{
	if (iterator != NULL) 
	{

		list_apply(iterator->location_stack,free);
		
		destroy_list(iterator->location_stack);
		destroy_list(iterator->node_stack);
		free(iterator);
	}
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_iterator_next
*
*	DESCRIPTION:
*		Returns the next element in the word tree.
*
*	\param iterator
*
*	\return A pointer to the next element.
***********************************************************/
void * ascii_tree_iterator_next(ascii_tree_iterator * iterator)
{
	uint16_t * index;
	ascii_tree_node * node;
	void * data;
	
	// Initialize
	data = NULL;

	// Error Check.
	if (iterator == NULL || iterator->node_stack == NULL || iterator->location_stack == NULL)
	{
		return NULL;
	}
	
	// Get the last index and node.
	index = (uint16_t*)         stack_pop(iterator->location_stack);
	node  = (ascii_tree_node *) stack_pop(iterator->node_stack);

	// Increament
	(*index) = (*index) + 1;

	while (node != NULL)
	{
		//Search the rest of the nodes down this path.
		for (;(*index)<ASCII_TREE_SIZE;(*index)++)
		{
			// If their is another node we need to to take that path
			if (node->nodes[*index] != NULL)
			{
				// Save current so we can go back.
				stack_push(iterator->location_stack,index);
				stack_push(iterator->node_stack,node);

				// Check to see if we need to return data.
				if (node->nodes[*index]->data != NULL)
				{
					// Save the data to return.
					data = node->nodes[*index]->data;
				}

				// Update to the next node and index.
				node = node->nodes[(*index)];
				index = (uint16_t*) malloc(sizeof(uint16_t));
				(*index) = -1;				
	
				// Check if data needs to be returned.
				if (data != NULL)
				{
					// Save the next state.
					stack_push(iterator->location_stack,index);
					stack_push(iterator->node_stack,node);

					// Delete the memory used by the index pointer.
					free(index);	//No Leakage.
					//return.
					return data;
				}
			}
		}
		// Delete the memory used by the index pointer.
		free(index);	//No Leakage.

		// Get the last index and node.
		index = (uint16_t*)         stack_pop(iterator->location_stack);
		node  = (ascii_tree_node *) stack_pop(iterator->node_stack);

		// If the index is not null we need to increment to the next position.
		if (index != NULL)
		{
			(*index) = (*index) + 1;
		}
	}

	return data;
}

/***********************************************************
*
*	PROCEDURE NAME:
*		ascii_tree_iterator_get_location
*
*	DESCRIPTION:
*		Returns the string of where the word tree is.
*		You are responcible for destroying that string.
*
***********************************************************/
void ascii_tree_iterator_get_location(ascii_tree_iterator * iterator,uint8_t * str, uint32_t n)
{
	uint32_t depth;
	uint32_t depth_sav;
	list_iterator * l_iter;
	
	depth = list_get_count(iterator->location_stack);

	// Prevent the string from overflowing.
	if ( depth > n ) return;

	depth_sav = depth;
	depth--;

	l_iter = create_list_iterator(iterator->location_stack);
	
	while (list_iterator_has_next(l_iter))
	{
		str[depth] = ascii_tree_get_char_value(*((uint8_t*) (list_iterator_next(l_iter))));
		depth--;
	}
	str[depth_sav-1]= '\0';
	destroy_list_iterator ( l_iter );
}

/*!*****************************************************
*	PROCEDURE NAME:
*		ascii_tree_iteratro_get_depth
*
*	DESCRIPTION:
*		Gets how far down tree the iterator is.
*
*	\brief
*		Gets how far down tree the iterator is.
*
*//******************************************************/
uint32_t ascii_tree_iterator_get_depth ( ascii_tree_iterator * iterator )
{
	return list_get_count(iterator->location_stack);
}

#ifdef __cplusplus
}
#endif


