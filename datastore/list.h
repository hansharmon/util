/*!*****************************************************************************
*
*	FILE NAME:
*		list.h
*
*	DESCRIPTION:
*		List that stores void pointers.
*		Provides a queue and stack implementation,
*		as well as iterators for scaning the linked list.
*		This is a double linked circular list so there is constant
*		time on most add and remove operations.  
*		Use the iterators to move through the list at
*		a constant rate.
*   
*	NOTES:
*
*	\brief Double Linked List
*
*	\file list.h
*
*//*****************************************************************************/
#ifndef __LIST_H__
#define __LIST_H__

/*----------------------------------------------------
                  INCLUDE FILES
  ---------------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

/*----------------------------------------------------
                  CONSTANT LITERALS
  ---------------------------------------------------*/
#ifndef NULL
	#define 	NULL 			0
#endif

#ifndef VOID_POINTER
	#define VOID_POINTER void *
#endif 

#ifdef __cplusplus
extern "C"
{
#endif

/*----------------------------------------------------
                      TYPES
  ---------------------------------------------------*/

//! Node to use for storing data.
typedef struct _list_node
{
	VOID_POINTER data;			//!< What to store at this location in the list.
	struct _list_node * next;		//!< Next node in the list.
	struct _list_node * previous;	//!< Preivous node in the list.
} list_node;

//! List for storing whatever.
typedef struct _list
{
	struct _list_node * head;				//!< Start of the list.
	uint32_t count;						//!< Number of Items in the list.

// Determines if we can actually keep an internal iterator or not.
#ifndef NON_ANSI_COMPLIANT
	struct _list_iterator * iterator;	//!< An iterator that is not thread safe, but does not require the creation and destruction of iterators.
#endif
} list;

//! Iterator for a list.
typedef struct _list_iterator
{
	struct _list * list;			//!< List this is an iterator for.
	struct _list_node * current;		//!< Current node pointed at.
} list_iterator;



//Create alternate definiations.
typedef list p_queue;		//!< List as a queue.
typedef list p_stack;		//!< List as a stack.


/*----------------------------------------------------
                   FUNCTION PROTOTYPES
  ----------------------------------------------------*/

/*!**********************************************************
*
*	PROCEDURE NAME:
*		create_node
*
*	DESCRIPTION:
*		Creates a blank node and returns it.
*		To destroy just use free(node).
*
*	\return A new list_node pointer.
*//**********************************************************/
list_node * create_list_node();

/*---------------------------------------------------------
 *                COMMON LIST-ISH FUNCTIONS
 *---------------------------------------------------------*/

/*!**********************************************************
*
*	PROCEDURE NAME:
*		create_list
*
*	DESCRIPTION:
*		Creates a pointer to a list
*
*	\return a pointer to a list.
*//**********************************************************/
list * create_list();

/*!**********************************************************
*
*	PROCEDURE NAME:
*		destroy_list
*
*	DESCRIPTION:
*		Free memory associated with the list.
*
*	\param list	The list to clean up.
*//**********************************************************/
void destroy_list(list * list);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_clear
*
*	DESCRIPTION:
*		Deletes all of the nodes within this list.
*
*	\param list	List to free all of the nodes
*//**********************************************************/
void list_clear(list * list);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_is_empty
*
*	DESCRIPTION:
*		Returns if the list is empty.
*
*	\param list	List to check for emptiness.
*
*	\return true if the list is empty.
*//**********************************************************/
uint8_t  list_is_empty(list * list);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_get_count
*
*	DESCRIPTION:
*		Gets the number of items stored in the list.
*
*	\param list	List to get the count from.
*
*	\return The number of items in the list.
*//**********************************************************/
uint32_t list_get_count(list * list);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_get
*
*	DESCRIPTION:
*		Returns the data found at index or NULL if not found.
*
*	\param list 	List to get an item from.
*	\param index	Index into the list get the item.
*
*	\return A pointer to the item or NULL if not found.
*//**********************************************************/
VOID_POINTER list_get(list * list,uint32_t index);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_whereis
*
*	DESCRIPTION:
*		Returns the location of the item given, or -1 if the item is not found.
*
*	\param list 	List to get a node from.
*	\param data		Item to find in the list.
*
*	\return A pointer to the node or NULL if not found.
*//**********************************************************/
int32_t list_whereis(list * list,VOID_POINTER data);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_get_node
*
*	DESCRIPTION:
*		Returns the node found at index or NULL if not found.
*		Helper for a bunch of functions.
*		Should not be used directly.
*
*	\param list 	List to get a node from.
*	\param index	Index into the list get the node.
*
*	\return A pointer to the node or NULL if not found.
*//**********************************************************/
list_node * list_get_node(list * list,uint32_t index);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_insert
*
*	DESCRIPTION:
*		Inserts data at the given index, 
*		into the list, or at the last available possition.
*		All other data is pushed back.
*
*	\param list		List to insert an item into.
*	\param index	Where to insert at.
*	\param data		Pointer to data to store.
*//**********************************************************/
void list_insert(list * list, uint32_t index,VOID_POINTER data);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_erase
*
*	DESCRIPTION:
*		Removes the data at the given index from the list, 
*		returns the item or NULL if not found.
*
*	\param list		List to remove an item from.
*	\param index	Index at which to remove the item.
*//**********************************************************/
VOID_POINTER list_erase(list * list, uint32_t index);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_add
*
*	DESCRIPTION:
*		Adds the data to the list/queue/statck.
*		Same as enqueue.
*
*	\param list		List to add another item to.
*	\param data		Item to add to the list.
*//**********************************************************/
void list_add(list * list,VOID_POINTER data);

#ifndef NON_ANSI_COMPLIANT
/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_print
*
*	DESCRIPTION:
*		Print everything out to file.
*		Use stdout to print to screen.
*
*	\param list		List to print.
*	\param output	Where to print to.
*	\param print_me	Function used to print the data.
*//**********************************************************/
void list_print(list * list,FILE* output,void (*print_me)(FILE*,void*));

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_apply
*
*	DESCRIPTION:
*		Calls the given function on elements in the list.
*
*	\param list			List to apply function to.
*	\param function		Function to apply to the list
*//**********************************************************/
void list_apply(list * list,void (*function)(VOID_POINTER));
#endif // NON_ANSI_COMPLIANT

/*---------------------------------------------------------
 *                 QUEUE FUNCTIONS
 *---------------------------------------------------------*/

/*!**********************************************************
*
*	PROCEDURE NAME:
*		enqueue
*
*	DESCRIPTION:
*		Adds the data to the end of the queue.
*		O(1) complexity for insert.
*
*	\param queue 	Queue to add an item to.
*	\param data		Data to add to the queue.
*//**********************************************************/
void enqueue (p_queue * queue, VOID_POINTER data);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		dequeue
*
*	DESCRIPTION:
*		Removes the first entry in the queue
*
*	\param queue	Queue to remove the first in value from.
*	
*	\return A point to the last item stored here.
*//**********************************************************/
VOID_POINTER dequeue(p_queue * queue);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_top
*
*	DESCRIPTION:
*		Returns the first element in the queue.
*
*	\param queue 	Queue to get the front of, or top of stack,etc.
*
*	\return A pointer to the data pointed to by the head node.
*//**********************************************************/
VOID_POINTER list_top(p_queue * queue);

/*----------------------------------------------------------
 *				STACK FUNCTIONS
 *----------------------------------------------------------*/

/*!**********************************************************
*
*	PROCEDURE NAME:
*		stack_push
*
*	DESCRIPTION:
*		Pushes an item to the front of the list.
*		Same as insert 0.
*
*	\param stack	Stack to push an item onto.
*	\param data		Data to push on to the stack.
*//**********************************************************/
void stack_push(p_stack * stack,VOID_POINTER data);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		stack_pop
*
*	DESCRIPTION:
*		Pop the last item added.
*		Same as dequeue.
*
*	\param stack	Stack to pop the first item off of.
*	
*	\return A pointer to the first item in the stack.
*//**********************************************************/
VOID_POINTER stack_pop(p_stack * stack);

/*-----------------------------------------------------------------------------
                               ITERATOR FUNCTIONS.
-----------------------------------------------------------------------------*/

/*!**********************************************************
*
*	PROCEDURE NAME:
*		create_list_terator
*
*	DESCRIPTION:
*		Creates a new list iterator.
*
*	\param list	List to create an iterator for.
*//**********************************************************/
list_iterator * create_list_iterator(list * list);


/*!**********************************************************
*
*	PROCEDURE NAME:
*		destroy_list_iterator
*
*	DESCRIPTION:
*		Destroys a list iteratot, can just use free.
*
*	\param iterator Iterator to destroy.
*
*//**********************************************************/
void destroy_list_iterator(list_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_rewind
*
*	DESCRIPTION:
*		returns the list iterator to initial state.
*
*	\param iterator Iterator to rewind.
*
*//**********************************************************/
void list_iterator_rewind(list_iterator * iterator);


/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_has_next
*
*	DESCRIPTION:
*		Check to see if there is a next element in the list.
*
*	\param iterator Iterator to check for next element.
*
*	\return 1 if the list has another element 0 if there are no more elements.
*//**********************************************************/
uint8_t  list_iterator_has_next(list_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_next
*
*	DESCRIPTION:
*		Gets the next element in the list or returns NULL if there is nothing left.
*
*	\param iterator Iterator to next set of data.
*
*	\return NULL if there is nothing left
*//**********************************************************/
VOID_POINTER list_iterator_next(list_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_has_previous
*
*	DESCRIPTION:
*		Check for previous elements.
*
*	\param iterator iterator to check for previous values.
*
*	\return true if the iterator is not pointing at the head of the list.
*//**********************************************************/
uint8_t  list_iterator_has_previous(list_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_previous
*
*	DESCRIPTION:
*		Retruns the previous element, moves the pointer.
*
*	\param iterator iterator to get the previous element or NULL if there is none.
*//**********************************************************/
VOID_POINTER list_iterator_previous(list_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_current
*
*	DESCRIPTION:
*		Returns the current element being looked at.
*
*	\param iterator Iterator to get the current element.
*
*	\return The current element, or NULL if there are problems.
*//**********************************************************/
VOID_POINTER list_iterator_current(list_iterator * iterator);

/*!**********************************************************
*
*	PROCEDURE NAME:
*		list_iterator_revmoe_current
*
*	DESCRIPTION:
*		Removes the current item from the list.
*		Uses the iterators current location to remove from,
*		then moves to the next item.
*
*	\brief Removes the current item from the list.
*
*	\param iterator Iterator to get the current element.
*
*	\return The current element, or NULL if there are problems.
*//**********************************************************/
VOID_POINTER list_iterator_remove_current(list_iterator * iterator);

#ifdef __cplusplus
}
#endif


#endif

