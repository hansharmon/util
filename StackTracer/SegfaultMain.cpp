#include "StackTracer.h"

#include<stdexcept>

/**
 * @brief Generates a segfault
 * Creates a segfault as some depth of calls.
 * This helps show how far into the stack this can trace.
 * 
 * @param depth How far into the stack before generating a segfault.
 */
void segfault(unsigned int depth) {
    if (depth != 0) {
        segfault(depth - 1);
    }
    char * boom = nullptr;
    boom[293890] = 'a';
}

/**
 * @brief Generates an exception
 * Creates an exception as some depth of calls.
 * This helps show how far into the stack this can trace.
 * 
 * @param depth How far into the stack before generating an exception.
 */
void exception(unsigned int depth) {
    if (depth != 0) {
        exception(depth - 1);
    }
    throw std::runtime_error("BOOM!");
}

/**
 * @brief Test main
 * Calls either segfault or exception depending on user input.
 * 
 * @param argn Number of elements in the arguments.
 * @param args Arguments, use depth and type [ 's' or  'x']
 * @return int Always returns 0.
 */
int main(int argn, char * args[]) {
    int depth = 10;
    char type = 'x';
    if (argn < 3) {
        printf ("Usage %s [depth] [type]\n\tdepth - stack depth to test\n\ttype - 's' for segfault, 'x' for exception\n", args[0]);
        return 0;
    }
    depth = atoi(args[1]);
    type = args[2][0];

    StackTracer::Instance().UseDefaults();
    if (type == 's') {
        segfault(depth);
    } else {
        exception(depth);
    }
    return 0;
}

