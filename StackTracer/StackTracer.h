#ifndef _STACK_TRACER_H_
#define _STACK_TRACER_H_

#include<string>
#include<signal.h>
#include<vector>

class StackTracer {
    public:
        /**
         * @brief Returns a static instance of the stack tracer.
         * Stack Tracer should be used as a singleton.
         * 
         * @return StackTracer& 
         */
        static StackTracer& Instance();

        /**
         * @brief Sets the stack trace to use default settings
         * Stack trace will be dumped to standard error (stderr)
         * After stack trace is dumped, the program will exit.
         * 
         */
        void UseDefaults();

        /**
         * @brief Sets which signals are to be caught.
         * Input signals like SIGINT, SIGSEGV, SIGABORT, etc.
         * 
         * @param signals 
         * @return StackTracer& 
         */
        StackTracer& CatchSignals(const std::vector<int> signals);


        /**
         * @brief Setups up the function to call with stack information when a signal trips
         * Default to DefaultPrinterHandler
         * @param printer Function to call for printing out the signal
         */
        StackTracer& SetSignalPrintHandler(void (*printer)(const std::string &stacktrace));

        /**
         * @brief Set the Signal Done Handler object
         * Sets what to do after the signal is caught.
         * By default this is an empty function.
         * 
         * @param on_done 
         * @return StackTracer 
         */
        StackTracer& SetSignalDoneHandler(void (*on_done)(int));

        /**
         * @brief Begins watching for errors.
         * 
         */
        void Begin();


        /**
         * @brief Default Done Handler, does nothing, allows code to exit.
         * 
         * @param signal 
         */
        static void DefaultDoneHandler(int signal);

        /**
         * @brief Default print handler, prints to standard error.
         * 
         * @param stacktrace 
         */
        static void DefaultPrinterHandler(const std::string &stacktrace);

        /**
         * @brief Gets the current stack trace as a dump.
         * Can be used at any time for some type of debugging.
         * 
         * @param depth Max depth of the callstack, defaults to 10.
         * @return std::string The call stack as a formatted string.
         */
        static std::string GetCallStack(int depth=100);

        /**
         * @brief Demangles a mangled function name.
         * In .so's function names for C++ can be cryptic, this helps to remove some of the confusion.
         * 
         * @param name Name of a function.
         * @return std::string 
         */
        static std::string GetDemangledFunctionName(const std::string &name);

        /**
         * @brief Gets called when a specified signal is generated.
         * 
         * @param signal Number of the signal called.
         */
        static void SignalHandler(int signal);

    protected:
        void (*printer)(const std::string &stacktrace);     //!< Function to call with a stack trace
        void (*on_done)(int);                              //!< Function to call after stack trace is finished.
        std::vector<int> signals;                           //!< Signals to catch for stack traces.

    private:
        static StackTracer INSTANCE;        //!< Actuall existing instance of the stack tracer.
        static const std::vector<int> DEFAULT_SIGNALS;

        StackTracer();       // Private constructor, use the get instance method instead.
        ~StackTracer();
};


#endif // _STACK_TRACER_H_

