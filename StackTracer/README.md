### Stack Tracer ###

This is for finding and printing stack traces for a c/c++ program
Include this library with your projects and then add into main 
Note you cannot optimize code or strip out symbol or this won't do any good.
Useful only for debugging, will not help with release builds.
This also contains a singleton for ease of use.

Note need to use the following compiler flags to generate somewhat readable output:  -rdynamic -pg -g3 -O0


### Compiling Test Code ###
  Compile:   g++ -rdynamic -pg -g3 -O0 -o segfault SegfaultMain.cpp StackTracer.cpp

### Running Test Program ###
  Run:  segfault [depth] [type]
    depth = how large of a stack to truncatechars_html
    type = 's' for segault, 'x' for exception

### Example 1 ###
At the simpliest form include the header file and call the Singleton and compile in the StackTracer.cpp code

  #include <StackTracer.h>

  // Use this for a simple case.
  StackTracker::Instance().Begin();
  
### Example 2 ###
  #include <StackTracer.h>

void my_handler(const &)

  StackTracker::Instance()
    .CatchSignals({ SIGINT, SIGABRT });
    .SetSignalPrintHandler(my_handler));
    .SetSignalDoneHandler(my_exit));
    .Begin());


### Comments ###
If there is a better c/c++ stack tracer out there, let me know, this is always a useful thing.
