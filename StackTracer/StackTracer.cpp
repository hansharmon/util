#include "StackTracer.h"
#include <iostream>
#include <string>
#include <sstream>
#include <cxxabi.h>
#include <execinfo.h>

StackTracer& StackTracer::Instance() {
    return INSTANCE;
}

/**
 * @brief Sets the stack trace to use default settings
 * Stack trace will be dumped to standard error (stderr)
 * After stack trace is dumped, the program will exit.
 * 
 */
void StackTracer::UseDefaults() {
    CatchSignals(DEFAULT_SIGNALS);
    SetSignalDoneHandler(StackTracer::DefaultDoneHandler);
    SetSignalPrintHandler(StackTracer::DefaultPrinterHandler);
    Begin();
}

/**
 * @brief Sets which signals are to be caught.
 * Input signal like SIGINT, SIGSEGV, SIGABORT, etc.
 * 
 * @param signals 
 * @return StackTracer& 
 */
StackTracer& StackTracer::CatchSignals(const std::vector<int> signals) {
    this->signals = signals;
    return *this;
}

/**
 * @brief Setups up the function to call with stack information when a signal trips
 * Default to DefaultPrinterHandler
 * @param printer Function to call for printing out the signal
 */
StackTracer& StackTracer::SetSignalPrintHandler(void (*printer)(const std::string &stacktrace)) {
    this->printer = printer;
    return *this;
}

/**
 * @brief Set the Signal Done Handler object
 * Sets what to do after the signal is caught.
 * By default this is an empty function.
 * 
 * @param on_done 
 * @return StackTracer 
 */
StackTracer& StackTracer::SetSignalDoneHandler(void (*on_done)(int)) {
    this->on_done = on_done;
    return *this;
}

/**
 * @brief Begins watching for errors.
 * 
 */
void StackTracer::Begin() {
    for (auto snum : signals) {
        signal(snum,StackTracer::SignalHandler);
    }
}

/**
 * @brief Default Done Handler, exits the code with a -1 for error.
 * 
 * @param signal 
 */
void StackTracer::DefaultDoneHandler(int signal) {
    exit(-1);
}

/**
 * @brief Default print handler, prints to standard error.
 * 
 * @param stacktrace 
 */
void StackTracer::DefaultPrinterHandler(const std::string &stacktrace) {
    std::cout << stacktrace << std::endl;
}

/**
 * @brief Gets the current stack trace as a dump.
 * Can be used at any time for some type of debugging.
 * 
 * @param depth Max depth of the callstack, defaults to 10.
 * @return std::string The call stack as a formatted string.
 */
std::string StackTracer::GetCallStack(int depth) {
 	std::stringstream output;
	void *buffer[depth];
	if (depth > depth) //restrict call stack depth to 10
		depth = depth;
	int actual_depth = backtrace(buffer, depth);
	char **stack = backtrace_symbols(buffer, actual_depth);
	if (stack)
	{
		output.str("\n");
		output << "[\n";
		for (int i = 0; i < actual_depth; i++)
		{
			output << "\t" << GetDemangledFunctionName(stack[i]) << "\n";
		}
		free(stack);
		output << "]\n";
	}
	else
		output << "Failed to get call stack";

	return output.str();
}

/**
 * @brief Demangles a mangled function name.
 * In .so's function names for C++ can be cryptic, this helps to remove some of the confusion.
 * 
 * @param name Name of a function.
 * @return std::string 
 */
std::string StackTracer::GetDemangledFunctionName(const std::string &name) {
    char *begin_name   = nullptr;
    char *begin_offset = nullptr;
    char *end_offset   = nullptr;
	size_t funcnamesize = 1024;
	char* funcname = (char*)malloc(funcnamesize);

	for (char *p = (char *)name.c_str(); *p; ++p)
	{
		if (*p == '(')
			begin_name = p;
		else if (*p == '+')
			begin_offset = p;
		else if (*p == ')' && begin_offset)
		{
			end_offset = p;
			break;
		}
	}
	if (begin_name && begin_offset && end_offset && begin_name < begin_offset)
	{
		*begin_name++ = '\0';
		*begin_offset++ = '\0';
		*end_offset = '\0';

		// mangled name is now in [begin_name, begin_offset) and caller
		// offset in [begin_offset, end_offset). now apply
		// __cxa_demangle():

		int status;
		char* ret = __cxxabiv1::__cxa_demangle(begin_name, funcname, &funcnamesize, &status);
		if (status == 0)
		{
			funcname = ret; // use possibly realloc()-ed string
			//printf("  %s : %s+%s\n", func.c_str(), funcname, begin_offset);
			std::string demangled = ret;
			free(ret);
			if (std::string::npos == demangled.find("\0")) // If there's a null character
			{
				return demangled.substr(0, name.size()-1);	// Change it to a semicolon
			}
			else
			{
				return demangled;
			}
		}
		else
		{
			free(funcname);
			// replace unprintable characters with periods
			std::string updatedFuncName = name;
			for (unsigned int i=0;i<name.length();++i) {
				if (!isprint(((int)name[i]))) {
					updatedFuncName[i] = '.';
				}
			}
			return updatedFuncName;		}
	}
	else
	{
		free(funcname);
		// couldn't parse the line? print the whole line.
		if (std::string::npos == name.find("\0")) // If there's a null character
		{
			return name.substr(0, name.size()-1);	// Change it to a semicolon
		}
		else
		{
			return name;
		}
	}
}


/**
 * @brief Gets called when a specified signal is generated.
 * 
 * @param signal Number of the signal called.
 */
void StackTracer::SignalHandler(int signal) {
    INSTANCE.printer(INSTANCE.GetCallStack());
    INSTANCE.on_done(signal);
}


StackTracer StackTracer::INSTANCE;        //!< Actual existing instance of the stack tracer.
const std::vector<int> StackTracer::DEFAULT_SIGNALS = {SIGSEGV,SIGABRT};

StackTracer::StackTracer() {
    CatchSignals(DEFAULT_SIGNALS);
    SetSignalDoneHandler(StackTracer::DefaultDoneHandler);
    SetSignalPrintHandler(StackTracer::DefaultPrinterHandler);
};

// Private constructor, use the get instance method instead.
StackTracer::~StackTracer() {
    // Nothing
}

