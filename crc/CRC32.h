// This does one crc32...
// TODO:  Make this more generic.
#ifndef CRC32_H
#define CRC32_H

#include <cstdint>

class CRC32
{
public:
    CRC32();

	void Reset ();
	uint32_t AppendBytes ( const uint8_t * bytes, uint32_t length );

	uint32_t GetCRC () const;


	static uint32_t CalculateCRC32 ( const uint8_t * buffer, uint32_t length );

protected:
	uint32_t				m_CRC;					//!< Stored crc value

	static const uint32_t m_InitialCRC32Value;	//!< The initializtion value
	static const uint32_t	m_CRC32Table [ 256 ];	//!< Stores crcs of each byte for speed
};

#endif // CRC32_H
