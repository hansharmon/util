#include "CRC32.h"
#include <iostream>

using namespace std;

// Spits out the crc32 of a file.
int main (int argc, char* argv[]) {
    if (argc < 2) {
        cout << "Usage: " << endl
            << "\t" << argv[0] << "<filename>" << endl;
        return -1;
    }

    FILE * file = fopen (argv[1], "r");
    if (!file) {
        cout << "Cannot open " << argv[1] << endl;
        return -1;
    }
    CRC32 crc32;
    uint8_t buffer [1024];
    auto red = fread(buffer,1,1024,file);
    auto crc = crc32.AppendBytes(buffer,red);
    printf ("CRC32 of %ld bytes is %04X\n", red, crc );
    fclose(file);

    return 0;
}