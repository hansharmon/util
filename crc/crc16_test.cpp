#include "crc16.h"
#include <iostream>

using namespace std;

// Spits out the crc32 of a file.
int main (int argc, char* argv[]) {
    if (argc < 2) {
        cout << "Usage: " << endl
            << "\t" << argv[0] << " <filename>" << endl;
        return -1;
    }

    FILE * file = fopen (argv[1], "r");
    if (!file) {
        cout << "Cannot open " << argv[1] << endl;
        return -1;
    }
    cout << "Opening " << argv[1] << endl;
    uint8_t buffer [2048];
    size_t red = fread(buffer,1,1024,file);
    uint16_t crc = crc16(buffer,red-1,0x1021,0xFFFF);
    printf ("CRC16 of %ld bytes is %04X\n", red, crc );
    fclose(file);
    return 0;
}