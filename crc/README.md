### CRC ####
A generic crc16 calculation and a specific crc32
The compiled version of the crc16 is doing CRC16 CCITT-FALSE

### Compiling ###

g++ crc16.c crc16_test.cpp -o crc16_test
g++ CRC16.cpp CRC32_test.cpp -o crc32_test

### Running ###
./crc##_test <filename>

### Output ###
CRC of the the input file.

### Comments ###
The base crc16 can be changed to match other crc16 values by changing the polynomial and start value to match the algorithm.

